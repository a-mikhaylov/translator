class TranslateToAssemblerController < ApplicationController
  def index
    @json = Program.last
    @json = JSON.parse(@json[:json])
    if @json['assembler'].nil?
      @json['assembler'] = ''
    end
  end

  def translate
    @json = Program.last
    @json = JSON.parse(@json[:json])
    assembler = TranslateToAssembler.new(@json)
    @json = @json.merge(assembler.translate)
    text = Program.new({json: @json.to_json})
    text.save
    redirect_to :back
  end
end

module Expression


  private
  def return_lexeme # Возврат считанной лексемы
    @position -= 1
    @position -= 1 while @program[@position] != '('
    false
  end

  def read_lexeme # Чтение лексемы
    @position += 1 while @program[@position] == "\n"
    return '(1,1)' if @position == @program.size
    lexeme = ''
    while @program[@position] != ')'
      lexeme += @program[@position]
      @position += 1
    end
    @position += 1
    lexeme + ')'
  end

  def compare?(options={}) # Сравнение лексемы с шаблоном
    lex = options[:lexeme]
    lex = lex.split(',')
    lex[0][0] = lex[1][-1] = ''
    lex[0] = lex[0].to_i - 1
    lex[1] = lex[1].to_i - 1
    return return_lexeme unless @table["#{lex[0]}"][lex[1]] == options[:template]
    return true unless options[:push]
    @compare_type[-1].push(options[:template])
    true
  end

  def logical_constant? # Анализ логической константы
    if compare?({lexeme: read_lexeme, template: 'true'}) ||
        compare?({lexeme: read_lexeme, template: 'false'})
      @compare_type[-1].push('bool')
      true
    end
  end

  def statement? # Анализ высказывания
    return true if logical_constant?
    id = identifier
    if id
      return true if @table["#{id[0]}"][id[1]]['identified']
      Error.new({program: @program, position: @position, code: '019',
                 name: "#{@table["#{id[0]}"][id[1]]['type']} #{@table["#{id[0]}"][id[1]]['name']}"})
    end
    return true if compare?({lexeme: read_lexeme, template: '!'}) && statement?
    return false unless compare?({lexeme: read_lexeme, template: '('})
    if logical_comparison?
      return true if compare?({lexeme: read_lexeme, template: ')'})
      Error.new({program: @program, position: @position, code: '004'})
    end
  end

  def operand? # Анализ операнда
    return false unless statement?
    while compare?({lexeme: read_lexeme, template: '&', push: true}) &&
        compare?({lexeme: read_lexeme, template: '&', push: true})
      return false unless statement?
    end
    true
  end

  def logical_comparison? # Анализ логического сравнения
    temp_size = @compare_type[-1].size
    temp_position = @position
    unless operand?
      @position = temp_position
      @compare_type.pop while @compare_type[-1].size > temp_size
      return false
    end
    loop do
      until compare?({lexeme: read_lexeme, template: ';'}) ||
          compare?({lexeme: read_lexeme, template: ')'}) do
        if compare?({lexeme: read_lexeme, template: '|', push: true}) &&
            compare?({lexeme: read_lexeme, template: '|', push: true})
          unless operand?
            @position = temp_position
            @compare_type.pop while @compare_type[-1].size > temp_size
            return false
          end
        else
          @position = temp_position
          @compare_type.pop while @compare_type[-1].size > temp_size
          return false
        end
      end
      return_lexeme
      break
    end
    true
  end

  def comparison_sign? # Анализ знака сравнения
    if compare?({lexeme: read_lexeme, template: '<', push: true})
      compare?({lexeme: read_lexeme, template: '=', push: true})
      return true
    end
    if compare?({lexeme: read_lexeme, template: '>', push: true})
      compare?({lexeme: read_lexeme, template: '=', push: true})
      return true
    end
    if compare?({lexeme: read_lexeme, template: '!', push: true})
      return compare?({lexeme: read_lexeme, template: '=', push: true})
    end
    true if compare?({lexeme: read_lexeme, template: '=', push: true})
  end

  def mathematical_comparison? # Анализ математического сравнения
    temp_size = @compare_type[-1].size
    temp_position = @position
    unless arithmetic_expression? && comparison_sign?
      @position = temp_position
      @compare_type[-1].pop while @compare_type[-1].size > temp_size
      return false
    end
    return true if arithmetic_expression?
    Error.new({program: @program, position: @position, code: '009'})
  end

  def comparison_expression? # Анализ выражения сравнения
    mathematical_comparison? || logical_comparison?
  end

  def numeric_string? # Анализ числовой строки
    temp_lexeme = read_lexeme
    temp_lexeme[1] == '4' ? true : return_lexeme
  end

  def order_number? # Анализ порядка числа с плавающей точкой
    unless compare?({lexeme: read_lexeme, template: 'e'})
      temp = "(2,18)(2,8)(4,#{@table['3'].size + 1})"
      @program.insert(@position, temp)
      @position += temp.size
      @table['3'].push('0')
      return true
    end
    unless compare?({lexeme: read_lexeme, template: '+'}) ||
        compare?({lexeme: read_lexeme, template: '-'})
      Error.new({program: @program, position: @position, code: '021'})
      return false
    end
    numeric_string?
  end

  def integer? # Анализ целого числа
    return false unless numeric_string?
    @compare_type[-1].push('int')
    true
  end

  def real? # Анализ числа с плавающей точкой
    return false unless numeric_string?
    return return_lexeme unless compare?({lexeme: read_lexeme, template: '.'})
    numeric_string?
    order_number?
    @compare_type[-1].push('float')
  end

  def positive_number? # Анализ положительного числа
    real? || integer?
  end

  def negative_number? # Анализ отрицательного числа
    return false unless compare?({lexeme: read_lexeme, template: '('})
    return return_lexeme unless compare?({lexeme: read_lexeme, template: '-'})
    positive_number?
    return true if compare?({lexeme: read_lexeme, template: ')'})
    Error.new({program: @program, position: @position, code: '004'})
  end

  def number? # Анализ числа
    negative_number? || positive_number?
  end

  def factor? # Анализ множителя
    return true if number?
    id = identifier
    if id
      if @table["#{id[0]}"][id[1]]['identified']
        @compare_type[-1].push(@table["#{id[0]}"][id[1]]['type'])
        return true
      end
      name =
      Error.new({program: @program, position: @position, code: '019',
                 name: "#{@table["#{id[0]}"][id[1]]['type']} #{@table["#{id[0]}"][id[1]]['name']}"})
    end
    return false unless compare?({lexeme: read_lexeme, template: '('})
    if arithmetic_expression?
      return true if compare?({lexeme: read_lexeme, template: ')'})
      Error.new({program: @program, position: @position, code: '004'})
    end
  end

  def summary? # Анализ слагаемого
    return false unless factor?
    while compare?({lexeme: read_lexeme, template: '*', push: true}) ||
        compare?({lexeme: read_lexeme, template: '/', push: true})
      return false unless factor?
    end
    true
  end

  def arithmetic_expression? # Анализ арифметического выражения
    temp_size = @compare_type[-1].size
    unless summary?
      @compare_type.pop while @compare_type[-1].size > temp_size
      return false
    end
    while compare?({lexeme: read_lexeme, template: '+', push: true}) ||
        compare?({lexeme: read_lexeme, template: '-', push: true})
      unless summary?
        @compare_type.pop while @compare_type[-1].size > temp_size
        return false
      end
    end
    true
  end

  def expression? # Анализ выражения
    return true if comparison_expression? || arithmetic_expression?
    Error.new({program: @program, position: @position, code: '006'})
  end
end

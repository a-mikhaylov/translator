class IndexController < ApplicationController
  def index
  end

  def lexical_analyser
    @lexical_result = { text: params[:program] }
    lexical_analysis = LexicalAnalyser.new(@lexical_result[:text])
    @lexical_result = @lexical_result.update(lexical_analysis.analyse)
    text = Program.new({json: @lexical_result.to_json})
    text.save
    redirect_to syntactic_path
  end
end

class LexicalAnalyser
  def initialize(program)
    @program = program
    const = File.open(File.join(Rails.root, 'app/assets/config/const.txt'), 'r') do |file|
      file.read
    end
    const = const.split(/\n/)
    const.each_index do |i|
      const[i] = const[i].split
    end
    @table = Array.new(4)
    @table[0] = const[0] # Таблица ключевых слов
    @table[1] = const[1] # Таблица разделителей
    @table[2] = [] # Таблица идентификаторов
    @table[3] = [] # Таблица числовых констант
  end

  def get_result # Вывод результата лексического анализа в консоль
    hash = {}
    @table.each_index do |i|
      hash["#{i}"] = @table[i]
    end
    { program: @program, table: hash }
  end

  def analyse
    delete_tab
    @program.each_index do |i|
      @line = i
      read_line if @program[i].length != 0
    end
    @program = @program.join("\n")
    get_result
  end

  private
  def delete_tab # Удаление из текста программы комментариев и символов табуляции
    @program = @program.split(/\#(.)*/)
                   .join
                   .split(/\t+/)
                   .join
                   .split(/\n/)
  end

  def read_line # Чтение строки символов
    length_str = 0
    while length_str < @program[@line].length
      length_str = (@program[@line][length_str+1] =~ /\w/) ?
          line_analysis(length_str) :
          symbol_analysis(length_str)
    end
  end

  def line_analysis(begin_str) # Анализ лексемы состоящей из нескольких символов
    temp_str = @program[@line][begin_str]
    if temp_str =~ /\W/
      symbol_analysis(begin_str)
    else
      length_str = begin_str + 1
      while length_str < @program[@line].length
        if @program[@line][length_str] =~ /\w/
          temp_str += @program[@line][length_str]
          length_str += 1
        else
          break
        end
      end
      result = keyword_analysis(temp_str)
      @program[@line][begin_str...length_str] = result
      begin_str + result.length
    end
  end

  def symbol_analysis(begin_str) # Анализ лексемы состоящей из одного символа
    temp_str = @program[@line][begin_str]
    if temp_str =~ /\s/
      @program[@line][begin_str] = ''
      begin_str
    else
      result = delimiter_analysis(temp_str)
      @program[@line][begin_str] = result
      begin_str + result.length
    end
  end

  def keyword_analysis(analyzed_keyword) # Анализ ключевого слова
    @table[0].each_index do |i|
      return "(1,#{i+1})" if (@table[0][i] == analyzed_keyword)
    end
    (analyzed_keyword =~ /(\d){2,}/) ? constant_analysis(analyzed_keyword) : identifier_analysis(analyzed_keyword)
  end

  def delimiter_analysis(analyzed_delim) # Анализ разделителя
    @table[1].each_index do |i|
      return "(2,#{i+1})" if (@table[1][i] == analyzed_delim)
    end
    (analyzed_delim =~ /\d/) ? constant_analysis(analyzed_delim) : identifier_analysis(analyzed_delim)
  end

  def identifier_analysis(analyzed_id) # Анализ индентификатора
    @table[2].each_index do |i|
      return "(3,#{i+1})" if (@table[2][i][:name] == analyzed_id)
    end
    hash = Hash.new { |hash, key| hash[key] = nil }
    hash[:name] = analyzed_id
    hash[:type]
    hash[:identified]
    @table[2].push(hash)
    "(3,#{@table[2].size})"
  end

  def constant_analysis(analyzed_const) # Анализ числовой константы
    @table[3].each_index do |i|
      return "(4,#{i+1})" if (@table[3][i] == analyzed_const)
    end
    @table[3].push(analyzed_const)
    "(4,#{@table[3].size})"
  end
end
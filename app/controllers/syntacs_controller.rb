class SyntacsController < ApplicationController
  def index
    @json = Program.last
    @json = JSON.parse(@json[:json])
  end

  def new
    @json = Program.last
    @json = JSON.parse(@json[:json])
    @error = File.open(File.join(Rails.root, '/error.txt'), 'r') do |file|
      file.read
    end
    @error = @error.split("\n")
  end

  def parsing
    text = Program.last
    @json = JSON.parse(text[:json])
    parse = Parsing.new(@json)
    @json = @json.update(parse.analyse)
    text = Program.new({json: @json.to_json})
    text.save
    @error = File.open(File.join(Rails.root, '/error.txt'), 'r') do |file|
      file.read
    end
    if @error.empty?
      redirect_to translate_path
    else
      redirect_to error_path
    end
  end

  def parsing_error
    lexical_result = { text: params[:program] }
    lexical_analysis = LexicalAnalyser.new(lexical_result[:text])
    lexical_result = lexical_result.update(lexical_analysis.analyse)
    text = Program.new({json: lexical_result.to_json})
    text.save
    parsing
  end
end

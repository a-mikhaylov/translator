require_relative 'expression.rb'
require_relative 'error'

class Parsing

  include Expression

  def initialize(options)
    @program = options['program']
    @table = options['table']
    @position = 0
    @semantic_table = File.open(File.join(Rails.root, 'app/assets/config/semantic_table.txt'), 'r') do |file|
      file.read
    end
    File.open('error.txt', 'w')
    @compare_type = []
  end

  def analyse
    unless compare?({lexeme: read_lexeme, template: '{'})
      Error.new({program: @program, position: @position, code: '001'})
    end
    program
    unless compare?({lexeme: read_lexeme, template: '}'})
      read_lexeme
      Error.new({program: @program, position: @position, code: '002'})
    end
    @compare_type.delete_if { |elem| elem.size < 2 }
    semantic_analysis
    { program: @program, table: @table }
  end

  private

  def semantic_analysis # Семантический анализ текста программы
    @semantic_table = @semantic_table.split(/\n/)
    @semantic_table.each_index do |i|
      @semantic_table[i] = @semantic_table[i].split
    end
    @compare_type.each do |elem|
      elem.each_index do |i|
        if elem[i].size == 1 && !elem[i+1].nil? && elem[i+1].size == 1
          temp = elem[i+1]
          elem[i] += temp
          elem.delete_at(i+1)
        end
      end
    end
    @compare_type.each do |elem|
      while elem.size > 3 do
        elem.push(operation_analysis({right: elem.pop, sing: elem.pop, left: elem.pop,
                                     position: elem[0]}))
      end
    end
  end

  # Анализ типов операндов, над которыми выполняется действие
  def operation_analysis(options={})
    @semantic_table.each_index do |i|
      return @semantic_table[i][3] if @semantic_table[i][0] == options[:sing] &&
          @semantic_table[i][1] == options[:left] && @semantic_table[i][2] == options[:right]
    end
    @semantic_table.each_index do |i|
      if @semantic_table[i][0] == options[:sing] && @semantic_table[i][1] == options[:left]
        Error.new({program: @program, position: options[:position], code: '020',
                   name: @semantic_table[i][1] + '\' ' + options[:sing] + ' \'' + @semantic_table[i][2]})
        return nil
      end
    end
    Error.new({program: @program, position: options[:position], code: '000'})
  end

  def program
    loop do
      break unless description? || operator?
    end
  end

  def description? # Анализ описания переменных
    return false if @position > @program.size - 6
    type_now = type
    return type_now unless type_now
    identifier( { :type => type_now })
    identifier( { :type => type_now }) while compare?({lexeme: read_lexeme, template: ','})
    unless compare?({lexeme: read_lexeme, template: ';'})
      Error.new({program: @program, position: @position, code: '005'})
    end
    true
  end

  def type
    return 'int' if compare?({lexeme: read_lexeme, template: 'int'})
    return 'float' if compare?({lexeme: read_lexeme, template: 'float'})
    'bool' if compare?({lexeme: read_lexeme, template: 'bool'})
  end

  def identifier(options={}) # Анализ идентификатора
    (0...@table['2'].size).map do |i|
      next unless compare?({lexeme: read_lexeme, template: @table['2'][i]})
      if options[:type] && @table['2'][i]['type'].nil?
        @table['2'][i]['type'] = options[:type]
        return [2, i]
      elsif options[:type] && @table['2'][i]['type']
        Error.new({program: @program, position: @position, code: '017', name: @table['2'][i]['name']})
      elsif options[:type].nil? && @table['2'][i]['type'].nil?
        Error.new({program: @program, position: @position, code: '018', name: @table['2'][i]['name']})
        line_translation
      else
        return [2, i]
      end
    end
    false
  end

  def operator? # Анализ оператора
    return false if @position > @program.size - 6
    unless composite? || assignment? || if_operator? || for_operator? ||
        while_operator? || read_operator? || write_operator?
      return false
    end
    true
  end

  def composite? # Анализ составного оператора
    return false unless compare?({lexeme: read_lexeme, template: '{'})
    loop do
      break unless description? || operator?
    end
    return true if compare?({lexeme: read_lexeme, template: '}'})
    Error.new( { program: @program, position: @position, code: '002' })
  end

  def assignment? # Анализ оператора присваивания
    @compare_type.push([@position])
    id = identifier
    unless id
      @compare_type.pop
      return false
    end
    @compare_type[-1].push(@table["#{id[0]}"][id[1]]['type'])
    if compare?({lexeme: read_lexeme, template: 'ass', push: true})
      @table["#{id[0]}"][id[1]]['identified'] = 1 if expression?
      return true if compare?({lexeme: read_lexeme, template: ';'})
      Error.new( { program: @program, position: @position, code: '005' })
    else
      Error.new( { program: @program, position: @position, code: '012' })
    end
  end

  def if_operator? # Анализ условного оператора
    @compare_type.push([@position])
    unless compare?({lexeme: read_lexeme, template: 'if'})
      @compare_type.pop
      return false
    end
    unless compare?({lexeme: read_lexeme, template: '('})
      Error.new({program: @program, position: @position, code: '003'})
    end
    Error.new({program: @program, position: @position, code: '011'}) unless mathematical_comparison?
    unless compare?({lexeme: read_lexeme, template: ')'})
      Error.new({program: @program, position: @position, code: '004'})
    end
    Error.new({program: @program, position: @position, code: '007'}) unless operator?
    loop do
      break unless elseif_operator?
    end
    else_operator?
    return true if compare?({lexeme: read_lexeme, template: 'endif'})
    Error.new({program: @program, position: @position, code: '016'})
  end

  def elseif_operator? # Анализ альтернативного условия
    @compare_type.push([@position])
    unless compare?({lexeme: read_lexeme, template: 'elseif'})
      @compare_type.pop
      return false
    end
    unless compare?({lexeme: read_lexeme, template: '('})
      Error.new({program: @program, position: @position, code: '003'})
    end
    Error.new({program: @program, position: @position, code: '011'}) unless mathematical_comparison?
    unless compare?({lexeme: read_lexeme, template: ')'})
      Error.new({program: @program, position: @position, code: '004'})
    end
    return true if operator?
    Error.new( { program: @program, position: @position, code: '007' })
  end

  def else_operator? # Анализ альтернативного действия условного оператора
    @compare_type.push([@position])
    unless compare?({lexeme: read_lexeme, template: 'else'})
      @compare_type.pop
      return false
    end
    return true if operator?
    Error.new( { program: @program, position: @position, code: '007' })
  end

  def for_operator? # Анализ оператора фиксированного цикла
    @compare_type.push([@position])
    unless compare?({lexeme: read_lexeme, template: 'for'})
      @compare_type.pop
      return false
    end
    unless compare?({lexeme: read_lexeme, template: '('})
      Error.new({program: @program, position: @position, code: '003'})
    end
    Error.new({program: @program, position: @position, code: '013'}) unless assignment?
    unless compare?({lexeme: read_lexeme, template: 'to'})
      Error.new({program: @program, position: @position, code: '014'})
    end
    @compare_type.push([@position])
    Error.new({program: @program, position: @position, code: '011'}) unless mathematical_comparison?
    unless compare?({lexeme: read_lexeme, template: ';'})
      Error.new({program: @program, position: @position, code: '005'})
    end
    step?
    unless compare?({lexeme: read_lexeme, template: ')'})
      Error.new({program: @program, position: @position, code: '004'})
    end
    Error.new({program: @program, position: @position, code: '007'}) unless operator?
    return true if compare?({lexeme: read_lexeme, template: 'next'})
    Error.new( { program: @program, position: @position, code: '015' })
  end

  def step? # Анализ шага оператора фиксированного цикла
    @compare_type.push([@position])
    unless compare?({lexeme: read_lexeme, template: 'step'})
      @compare_type.pop
      return false
    end
    return true if !comparison_expression? && arithmetic_expression?
    Error.new( { program: @program, position: @position, code: '009' })
  end

  def while_operator? # Анализ оператора условного цикла
    @compare_type.push([@position])
    unless compare?({lexeme: read_lexeme, template: 'while'})
      @compare_type.pop
      return false
    end
    unless compare?({lexeme: read_lexeme, template: '('})
      Error.new({program: @program, position: @position, code: '003'})
    end
    Error.new({program: @program, position: @position, code: '010'}) unless mathematical_comparison?
    unless compare?({lexeme: read_lexeme, template: ')'})
      Error.new({program: @program, position: @position, code: '004'})
    end
    Error.new({program: @program, position: @position, code: '007'}) unless operator?
  end

  def read_operator? # Анализ оператора ввода
    return false unless compare?({lexeme: read_lexeme, template: 'read'})
    unless compare?({lexeme: read_lexeme, template: '('})
      Error.new({program: @program, position: @position, code: '003'})
    end
    Error.new({program: @program, position: @position, code: '008'}) unless identifier
    while compare?({lexeme: read_lexeme, template: ','})
      Error.new({program: @program, position: @position, code: '008'}) unless identifier
    end
    unless compare?({lexeme: read_lexeme, template: ')'})
      Error.new({program: @program, position: @position, code: '004'})
    end
    return true if compare?({lexeme: read_lexeme, template: ';'})
    Error.new( { program: @program, position: @position, code: '005' })
  end

  def write_operator? # Анализ оператора вывода
    @compare_type.push([@position])
    unless compare?({lexeme: read_lexeme, template: 'writeln'})
      return false
    end
    Error.new({program: @program, position: @position, code: '006'}) unless expression?
    while compare?({lexeme: read_lexeme, template: ','})
      @compare_type.push([@position])
      unless expression?
        @compare_type.pop
        Error.new({program: @program, position: @position, code: '006'})
      end
    end
    return true if compare?({lexeme: read_lexeme, template: ';'})
    Error.new( { program: @program, position: @position, code: '005' })
  end

  def line_translation
    @position += 1 while @program[@position] != "\n"
  end
end
class Error
  def initialize(options={})
    @program = options[:program]
    @position = options[:position]
    @error_code = options[:code]
    @error_name = options[:name]
    create_error
  end

  def create_error
    message = error_message(@error_code)
    error_log(message)
  end

  def error_message(code) # Формирование текста по коду ошибки
    case code
    when '001'
      "\tProgram(line #{line_error}) : error(0x#{code}) : syntax error : missing '{'"
    when '002'
      "\tProgram(line #{line_error}) : error(0x#{code}) : syntax error : missing '}'"
    when '003'
      "\tProgram(line #{line_error}) : error(0x#{code}) : syntax error : missing '('"
    when '004'
      "\tProgram(line #{line_error}) : error(0x#{code}) : syntax error : missing ')'"
    when '005'
      "\tProgram(line #{line_error}) : error(0x#{code}) : syntax error : missing ';'"
    when '006'
      "Program(line #{line_error}) : error(0x#{code}) : syntax error : missing 'expression'"
    when '007'
      "\tProgram(line #{line_error}) : error(0x#{code}) : syntax error : missing 'operator'"
    when '008'
      "\tProgram(line #{line_error}) : error(0x#{code}) : syntax error : missing 'identifier'"
    when '009'
      "\tProgram(line #{line_error}) : error(0x#{code}) : syntax error : missing 'arithmetic_expression'"
    when '010'
      "\tProgram(line #{line_error}) : error(0x#{code}) : syntax error : missing 'comparison expression'"
    when '011'
      "\tProgram(line #{line_error}) : error(0x#{code}) : syntax error : missing 'mathematical comparison'"
    when '012'
      "\tProgram(line #{line_error}) : error(0x#{code}) : syntax error : missing 'ass'"
    when '013'
      "\tProgram(line #{line_error}) : error(0x#{code}) : syntax error : missing 'assignment'"
    when '014'
      "\tProgram(line #{line_error}) : error(0x#{code}) : syntax error : missing 'to'"
    when '015'
      "\tProgram(line #{line_error}) : error(0x#{code}) : syntax error : missing 'next'"
    when '016'
      "\tProgram(line #{line_error}) : error(0x#{code}) : syntax error : missing 'endif'"
    when '017'
      "\tProgram(line #{line_error}) : error(0x#{code}) : repeated variable declaration : '#{@error_name}'"
    when '018'
      "\tProgram(line #{line_error}) : error(0x#{code}) : undefined variable : '#{@error_name}'"
    when '019'
      "\tProgram(line #{line_error}) : error(0x#{code}) : expected extern #{@error_name}"
    when '020'
      "\tProgram(line #{line_error}) : error(0x#{code}) : type mismatch : expected '#{@error_name}'"
    when '021'
      "\tProgram(line #{line_error}) : error(0x#{code}) : syntax error : missing '+' or '-'"
    else
      "\tProgram(line #{line_error}) : error(0x#{code}) : undefined error"
    end
  end

  def line_error #Получение номера строки, в которой найдена ошибка
    count = @program[0..@position].scan(/\n/)
    if @program[@position - 1] == "\n"
      count.size
    else
      count.size+1
    end
  end

  def error_log(description) #Вывод сообщения об ошибке в файл error.txt
    File.open('error.txt', 'a'){ |file| file.puts  description }
  end
end
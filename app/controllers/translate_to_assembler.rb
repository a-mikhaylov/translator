class TranslateToAssembler
  def initialize(options={})
    @program = options['program']
    @table = options['table']
    @position = 0
    @start_memory = 200
    @mark = 0
    @code = ''
  end

  def translate
    while @position < @program.size
      lexeme = read_lexeme
      if compare?({lexeme: lexeme, template: '{'}) || compare?({lexeme: lexeme, template: '}'}) ||
          compare?({lexeme: lexeme, template: 'int'}) || compare?({lexeme: lexeme, template: 'float'}) ||
          compare?({lexeme: lexeme, template: 'bool'}) || compare?({lexeme: lexeme, template: 'ass'}) ||
          compare?({lexeme: lexeme, template: 'next'}) || compare?({lexeme: lexeme, template: ';'})
        next
      end
      case_operator(get_word(lexeme))
    end
    @code += "HLT\n"
    @table['2'].pop
    { program: @program, table: @table, assembler: @code }
  end

  private

  def case_operator(word)
    case word
      when 'if'
        translate_if
      when 'for'
        translate_for
      when 'while'
        translate_while
      when 'read'
        translate_read
      when 'writeln'
        translate_write
      else
        translate_ass(word)
    end
  end

  def translate_composite # Перевод составного оператора
    lexeme = read_lexeme
    until compare?({lexeme: lexeme, template: '}'})
      case_operator(get_word(lexeme))
      lexeme = read_lexeme
    end
  end

  def translate_if # Перевод условного оператора
    state = 0
    next_mark = ''
    have_else = false
    while state != 3
      case state
      when 0
        translate = translate_comparison({end_comparison: ')'})
        next_mark = translate[:mark]
        @code += translate[:code]
        state += 1
      when 1
        lexeme = read_lexeme
        if compare?({lexeme: lexeme, template: '{'})
          translate_composite
          state += 1
          next
        end
        next if compare?({lexeme: lexeme, template: ')'})
        case_operator(get_word(lexeme))
        state += 1
      when 2
        lexeme = read_lexeme
        if compare?({lexeme: lexeme, template: 'elseif'})
          state = 0
          @code += "m#{next_mark}: "
          next
        end
        if compare?({lexeme: lexeme, template: 'else'})
          state = 1
          @code += "m#{next_mark}: "
          have_else = true
          next
        end
        if compare?({lexeme: lexeme, template: 'endif'})
          state += 1
          @code += "m#{next_mark}: " unless have_else
          next
        end
      end
    end
  end

  def translate_for # Перевод оператора фиксированного цикла
    state = 0
    begin_mark = exit_mark = temp_code = temp_identifier = ''
    while state != 5
      case state
        when 0
          read_lexeme
          temp_identifier = get_word(read_lexeme)
          translate_ass(temp_identifier)
          state += 1
        when 1
          @code += "m#{@mark}: "
          begin_mark = @mark
          @mark += 1
          read_lexeme
          translate = translate_comparison({end_comparison: ';'})
          exit_mark = translate[:mark]
          @code += translate[:code]
          state += 1
        when 2
          read_lexeme
          (compare?({lexeme: read_lexeme, template: 'step'})) ?
              temp_code = translate_expression({end_first: ')'}) :
              return_lexeme
          state += 1
        when 3
          lexeme = read_lexeme
          if compare?({lexeme: lexeme, template: '{'})
            translate_composite
            state += 1
            next
          end
          next if compare?({lexeme: lexeme, template: ')'})
          case_operator(get_word(lexeme))
          state += 1
        when 4
          unless temp_code.empty?
            @code += temp_code
            @code += "WR #{@start_memory + get_address(temp_identifier['name'])}\n"
          end
          @code += "JMP m#{begin_mark}\nm#{exit_mark}: "
          state += 1
      end
    end

  end

  def translate_while # Перевод оператора условного цикла
    state = 0
    exit_mark = ''
    @code += "m#{@mark}: "
    begin_mark = @mark
    @mark += 1
    while state != 3
      case state
      when 0
        translate = translate_comparison({end_comparison: ')'})
        exit_mark = translate[:mark]
        @code += translate[:code]
        state += 1
      when 1
        lexeme = read_lexeme
        if compare?({lexeme: lexeme, template: '{'})
          translate_composite
          state += 1
          next
        end
        next if compare?({lexeme: lexeme, template: ')'})
        case_operator(get_word(lexeme))
        state += 1
      when 2
        @code += "JMP m#{begin_mark}\nm#{exit_mark}: "
        state += 1
      end
    end
  end

  def translate_read # Перевод оператора ввода
    state = 0
    while state != 2
      lexeme = read_lexeme
      next if compare?({lexeme: lexeme, template: '('})
      case state
      when 0
        lexeme = get_word(lexeme)
        @code += "IN\nWR #{@start_memory + get_address(lexeme['name'])}\n"
        state += 1
      when 1
        if compare?({lexeme: lexeme, template: ','})
          state = 0
          next
        end
        state += 1
      end
    end
  end

  def translate_write # Перевод оператора вывода
    state = 0
    while state != 2
      case state
      when 0
        @code += translate_expression({end_first: ',', end_second: ';'})
        @code += "OUT\n"
        state += 1
      when 1
        if compare?({lexeme: read_lexeme, template: ','})
          state = 0
          next
        end
        state += 1
      end
    end
  end

  def translate_ass(identifier) # Перевод оператора присваивания
    id = false
    @table['2'].each do |elem|
      if elem == identifier
        id = true
        break
      end
    end
    return unless id
    state = 0
    while state != 2
      case state
      when 0
        (compare?({lexeme: read_lexeme, template: 'ass'})) ? state += 1 : return
      when 1
        @code += translate_expression({end_first: ';'})
        @code += "WR #{@start_memory + get_address(identifier['name'])}\n"
        state += 1
      end
    end
  end

  def translate_expression(options={}) # Перевод выражения
    expression = read_expression({end_first: options[:end_first], end_second: options[:end_second],
                                  end_third: options[:end_third], end_fourth: options[:end_fourth]})
    return if expression.empty?
    polisi = translate_to_postfix({expression: expression})
    code = ''
    temp_lexeme = []
    until polisi.empty?
      temp = polisi.shift
      case temp
      when '+'
        right = temp_lexeme.pop
        left = temp_lexeme.pop
        identifier?(left) ? code += "RD #{@start_memory + get_address(left)}\n" : code += "RD ##{left}\n"
        identifier?(right) ? code += "ADD #{@start_memory + get_address(right)}\n" : code += "ADD ##{right}\n"
        code += set_address(temp_lexeme)
        next
      when '-'
        right = temp_lexeme.pop
        left = temp_lexeme.pop
        identifier?(left) ? code += "RD #{@start_memory + get_address(left)}\n" : code += "RD ##{left}\n"
        identifier?(right) ? code += "SUB #{@start_memory + get_address(right)}\n" : code += "SUB ##{right}\n"
        code += set_address(temp_lexeme)
        next
      when '*'
        right = temp_lexeme.pop
        left = temp_lexeme.pop
        identifier?(left) ? code += "RD #{@start_memory + get_address(left)}\n" : code += "RD ##{left}\n"
        identifier?(right) ? code += "MUL #{@start_memory + get_address(right)}\n" : code += "MUL ##{right}\n"
        code += set_address(temp_lexeme)
        next
      when '/'
        right = temp_lexeme.pop
        left = temp_lexeme.pop
        identifier?(left) ? code += "RD #{@start_memory + get_address(left)}\n" : code += "RD ##{left}\n"
        identifier?(right) ? code += "DIV #{@start_memory + get_address(right)}\n" : code += "DIV ##{right}\n"
        code += set_address(temp_lexeme)
        next
      when '&&'
        right = temp_lexeme.pop
        left = temp_lexeme.pop
        if identifier?(left)
          code += "RD #{@start_memory + get_address(left)}\n"
        else
          (left == 'true') ? code += "RD #1\n" : code += "RD #0\n"
        end
        if identifier?(right)
          code += "AND #{@start_memory + get_address(right)}\n"
        else
          (right == 'true') ? code += "AND #1\n" : code += "AND #0\n"
        end
        code += set_address(temp_lexeme)
        next
      when '||'
        right = temp_lexeme.pop
        left = temp_lexeme.pop
        if identifier?(left)
          code += "RD #{@start_memory + get_address(left)}\n"
        else
          (left == 'true') ? code += "RD #1\n" : code += "RD #0\n"
        end
        if identifier?(right)
          code += "OR #{@start_memory + get_address(right)}\n"
        else
          (right == 'true') ? code += "OR #1\n" : code += "OR #0\n"
        end
        code += set_address(temp_lexeme)
        next
      when '!'
        lexeme = temp_lexeme.pop
        if identifier?(lexeme)
          code += "RD #{@start_memory + get_address(lexeme)}\n"
        else
          (lexeme == 'true') ? code += "RD #1\n" : code += "RD #0\n"
        end
        code += "NOT\n"
        code += set_address(temp_lexeme)
        next
      else
        temp_lexeme.push(temp)
      end
    end
    (0...@table['2'].size).map do |i|
      if @table['2'][i]['name'] =~ /M\d+/ && @table['2'][i]['name'] != 'M1'
        @table['2'].delete_at(i)
      end
    end
    temp_lexeme.each do |elem|
      unless elem =~ /M\d+/
        lexeme = temp_lexeme.pop
        identifier?(lexeme) ? code += "RD #{@start_memory + get_address(lexeme)}\n" : code += "RD ##{lexeme}\n"
        return code
      end
    end
    code[-1] = ''
    code[-1] = '' until code[-1] == "\n"
    code
  end

  def translate_to_postfix(options={}) # Перевод выражения в постфиксную форму записи
    result = []
    operation = []
    expression = options[:expression]
    expression.each_index do |i|
      out = nil
      @table['1'].each do |delimiter|
        if delimiter == expression[i][0]
          if expression[i] == '(' && expression[i+1] == '-'
            operation.push(expression[i])
            temp = [expression[i], 0, expression[i+1]]
            expression[i..i+1] = temp
            out = true
            break
          elsif expression[i] == '('
            operation.push(expression[i])
            out = true
            break
          elsif expression[i] == ')'
            temp = operation.pop
            until temp == '('
              result.push(temp)
              temp = operation.pop
            end
            out = true
            break
          else
            if operation.empty? ||
                ((expression[i] == '*' || expression[i] == '/') && (operation[-1] == '+' || operation[-1] == '-')) ||
                (expression[i] == '&&' && operation[-1] == '||') ||
                (expression[i] == '!' && (operation[-1] == '&&' || operation[-1] == '||'))
              operation.push(expression[i])
              out = true
              break
            end
            case expression[i]
            when '*' || '/'
              until operation[-1] == '+' || operation[-1] == '-' || operation[-1] == '(' || operation.empty?
                result.push(operation.pop)
              end
            when '+' || '-'
              until operation[-1] == '(' || operation.empty?
                result.push(operation.pop)
              end
            when '!'
              until operation[-1] == '&&' || operation[-1] == '||' || operation[-1] == '(' || operation.empty?
                result.push(operation.pop)
              end
            when '&&'
              until operation[-1] == '||' || operation[-1] == '(' || operation.empty?
                result.push(operation.pop)
              end
            when '||'
              until operation[-1] == '(' || operation.empty?
                result.push(operation.pop)
              end
            end
            operation.push(expression[i])
            out = true
            break
          end
        end
      end
      next if out
      result.push(expression[i])
    end
    result.push(operation.pop) until operation.empty?
    result
  end

  def translate_comparison(options={}) # Перевод выражения сравнения
    read_lexeme
    code = translate_expression({end_first: '<', end_second: '>', end_third: '=', end_fourth: '!'})
    first_mark = find_mark
    code += "WR #{first_mark['name']}\n"
    temp_sing = get_word(read_lexeme)
    lexeme = read_lexeme
    (compare?({lexeme: lexeme, template: '='})) ? temp_sing += get_word(lexeme) : return_lexeme
    code += translate_expression({end_first: options[:end_comparison]})
    second_mark = find_mark
    code += "WR #{second_mark['name']}\n"
    case temp_sing
      when '<'
        code += "RD #{first_mark['name']}\nSUB #{second_mark['name']}\nJNS m#{@mark}\n"
      when '<='
        code += "RD #{first_mark['name']}\nSUB #{second_mark['name']}\nJZ m#{@mark}\n"
        temp_mark = @mark
        @mark += 1
        code += "JNS m#{@mark}\nm#{temp_mark}: "
      when '>'
        code += "RD #{first_mark['name']}\nSUB #{second_mark['name']}\nJZ m#{@mark}\nJS m#{@mark}\n"
      when '>='
        code += "RD #{first_mark['name']}\nSUB #{second_mark['name']}\nJ'S m#{@mark}\n"
      when '!='
        code += "RD #{first_mark['name']}\nSUB #{second_mark['name']}\nJZ m#{@mark}\n"
      when '='
        code += "RD #{first_mark['name']}\nSUB #{second_mark['name']}\nJNZ m#{@mark}\n"
    end
    temp_mark = @mark
    @mark += 1
    {mark: temp_mark, code: code}
  end

  def read_lexeme #Чтение лексемы
    @position += 1 while @program[@position] == "\n"
    lexeme = @program[@position]
    @position += 1
    while @program[@position-1] != ')'
      lexeme += @program[@position]
      @position += 1
    end
    lexeme
  end

  def return_lexeme #Возврат считанной лексемы
    @position -= 1
    @position -= 1 while @program[@position] != '('
    false
  end
  
  def read_expression(options={}) # Чтение выражения
    bracket = 0
    expression = []
    lexeme = read_lexeme
    until (compare?({lexeme: lexeme, template: options[:end_first]}) ||
        compare?({lexeme: lexeme, template: options[:end_second]}) ||
        compare?({lexeme: lexeme, template: options[:end_third]}) ||
        compare?({lexeme: lexeme, template: options[:end_fourth]})) && bracket == 0
      bracket += 1 if compare?({lexeme: lexeme, template: '('})
      bracket -= 1 if compare?({lexeme: lexeme, template: ')'})
      if compare?({lexeme: lexeme, template: '.'})
        temp = lexeme.clone
        loop do
          expression[-1] += get_word(lexeme)
          lexeme = read_lexeme
          if compare?({lexeme: temp, template: '+'}) || compare?({lexeme: temp, template: '+'})
            expression[-1] += get_word(lexeme)
            break
          end
          temp = lexeme
        end
        lexeme = read_lexeme
      else
        if lexeme[1] == '3'
          variable = get_word(lexeme)
          expression.push(variable['name'])
          lexeme = read_lexeme
        else
          expression.push(get_word(lexeme))
          lexeme = read_lexeme
        end
      end
    end
    expression.each_index do |i|
      if expression[i] == '&'
        expression[i] += '&'
        expression.delete_at(i + 1)
        next
      end
      if expression[i] == '|'
        expression[i] += '|'
        expression.delete_at(i + 1)
        next
      end
    end
    return_lexeme
    expression
  end

  def compare?(options={}) # Сравнение лексемы с шаблоном
    lex = options[:lexeme]
    lex = lex.split(',')
    lex[0][0] = lex[1][-1] = ''
    lex[0] = lex[0].to_i - 1
    lex[1] = lex[1].to_i - 1
    @table["#{lex[0]}"][lex[1]] == options[:template] ? options[:template] : false
  end

  # Перевод лексемы из записи вида (номер таблицы, номер элемента) в строковое значение
  def get_word(lexeme)
    lexeme = lexeme.split(',')
    lexeme[0][0] = lexeme[1][-1] = ''
    lexeme[0] = lexeme[0].to_i - 1
    lexeme[1] = lexeme[1].to_i - 1
    @table["#{lexeme[0]}"][lexeme[1]]
  end

  def identifier?(name) # Является ли лексема индентификатором
    @table['2'].each do |elem|
      return true if name == elem['name']
    end
    false
  end

  def set_address(lexeme) # Создание переменной для хранения промежуточного результата
    i = 1
    lexeme.each do |elem|
      i += 1 if elem =~ /M\d+/
    end
    lexeme.push("M#{i}")
    @table['2'].each do |elem|
      return "WR #{@start_memory + get_address("M#{i}")}\n" if elem['name'] == "M#{i}"
    end
    @table['2'].push({'name' => "M#{i}"})
    "WR #{@start_memory + get_address("M#{i}")}\n"
  end

  def get_address(name) # Получить адрес, если лексема индентификатор
    @table['2'].each_index do |i|
      return i if name == @table['2'][i]['name']
    end
    false
  end

  def find_mark # Создание метки перехода
    i = 0
    @table['2'].each do |elem|
      i += 1 if elem['name'] =~ /M\d+/
    end
    @table['2'].push({ 'name' => "M#{i + 1}"})
    @table['2'][-1]
  end
end
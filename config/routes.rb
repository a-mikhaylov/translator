Rails.application.routes.draw do
  post 'index/lexical_analyser', as: 'lexical'

  get '/syntactic' => 'syntacs#index', as: 'syntactic'
  get '/error' => 'syntacs#new', as: 'error'
  post '/syntacs/parsing', as: 'parsing'
  post '/syntacs/parsing_error', as: 'parsing_error'

  get '/translate' => 'translate_to_assembler#index', as: 'translate'
  post '/translate/assembler' => 'translate_to_assembler#translate', as: 'assembler'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'index#index'
end

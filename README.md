# README

* Ruby version 2.3.0

* Rails version 5.0.0.1

* Project name "Translator"


* The application implements a logical, syntactic and semantic analysis of the text of the program, introduced in the modal language. In the absence of errors in the text, translate it into assembler
